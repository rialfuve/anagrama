package com.aval.controller;

import com.aval.model.Dato;

public class Validacion {
	
	public String validar(Dato palagras) {
		if (palagras.getPalabraUno().length() != palagras.getPalabraDos().length()) {
			return "NO es un ANAGRAMA";
		}
		for (int i = 0; i < palagras.getPalabraUno().length(); i++) {
            //si la palabraDos no contiene algun caracter de palabraUno, no es un anagrama y termina
            if (!palagras.getPalabraDos().contains(String.valueOf(palagras.getPalabraUno().charAt(i)))) {
                return "NO es un ANAGRAMA";
            }
        }
		return "SI es un ANAGRAMA";
	}

}
