package com.aval.model;

public class Dato {
	
	private String palabraUno;
	
	private String palabraDos;

	public String getPalabraUno() {
		return palabraUno;
	}

	public void setPalabraUno(String palabraUno) {
		this.palabraUno = palabraUno;
	}

	public String getPalabraDos() {
		return palabraDos;
	}

	public void setPalabraDos(String palabraDos) {
		this.palabraDos = palabraDos;
	}
	

}
