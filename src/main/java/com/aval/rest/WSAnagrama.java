package com.aval.rest;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.aval.controller.Validacion;
import com.aval.model.Dato;

@RestController

public class WSAnagrama {
	
	
	@RequestMapping(value = "/anagrama", method = RequestMethod.POST)
	public String validarPalabras(@RequestBody Dato peticion) {
		Validacion validar = new Validacion();
		return validar.validar(peticion);
	}
	
}
