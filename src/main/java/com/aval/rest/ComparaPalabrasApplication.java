package com.aval.rest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class ComparaPalabrasApplication {
	public static void main(String[] args) {
		SpringApplication.run(ComparaPalabrasApplication.class, args);
	}

}
